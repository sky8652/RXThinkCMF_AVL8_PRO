<?php 
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2020 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace App\Http\Controllers;


use App\Services\ExampleService;

/**
 * 案例演示管理-控制器
 * @author 牧羊人
 * @since: 2021/05/31
 * Class ExampleController
 * @package App\Http\Controllers
 */
class ExampleController extends Backend
{
    /**
     * 构造函数
     * @param Request $request
     * @since 2021/05/31
     * LevelController constructor.
     * @author 牧羊人
     */
    public function __construct()
    {
        parent::__construct();
        $this->service = new ExampleService();
    }

	                    	            
	/**
	 * 设置是否VIP
	 * @return mixed
	 * @since 2021/05/31
	 * @author 牧羊人
	 */
	public function setIsVip()
	{
        $result = $this->service->setIsVip();
        return $result;
	}
    	                        
}
