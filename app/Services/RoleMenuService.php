<?php
// +----------------------------------------------------------------------
// | RXThinkCMF框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2017~2021 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | Author: 牧羊人 <1175401194@qq.com>
// +----------------------------------------------------------------------

namespace App\Services;

/**
 * 角色菜单关系-服务类
 * @author 牧羊人
 * @since 2020/11/11
 * Class RoleMenuService
 * @package App\Services
 */
class RoleMenuService extends BaseService
{

}
